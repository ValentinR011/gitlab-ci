from flask import Flask, request
import redis, json, requests

app = Flask(__name__)
redis = redis.Redis(host='redis', port=6379)

@app.route('/healthz', methods = ['GET'])
def healthCheck():
    if redis.ping():
        response = 'PONG', 200
    else:
        response = 'Connection failed', 503
    return json.dumps(response)


@app.route('/user/<int:userId>', methods = ['GET'])
def isUserOnline(userId):
    if redis.exists(userId):
        return json.dumps("User {} is connected".format(userId)), 200

    return json.dumps("User {} is not connected".format(userId)), 404


@app.route('/user', methods = ['POST'])
def setUserOnline():
    userId = request.get_json()['userId']
    redis.set(userId, "", ex=30)
    return json.dumps("Status for user {} has been updated".format(userId)), 200