import requests, pytest
baseUrl = "http://localhost:5000"

def test_health_check():
    path = "/healthz"
    response = requests.get(baseUrl + path)
    assert response.status_code == 200
    assert response.json() == ['PONG', 200]

def test_set_user_online():
    path = "/user"
    response = requests.post(baseUrl + path, json={"userId": 1})
    assert response.status_code == 200
    assert response.json() == "Status for user 1 has been updated"

def test_get_user_online():
    path = "/user/1"
    response = requests.get(baseUrl + path)
    assert response.status_code == 200
    assert response.json() == "User 1 is connected"


